package vn.tbcs.sendmail.service.mapper.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Formater {
	public static String formatDecimal(String data) {
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');
		if (data.contains(".")) {
			DecimalFormat df = new DecimalFormat("#,###,###.00", otherSymbols);
			Double a = Double.parseDouble(data);
			String d = df.format(a);
			return d;
		} else {
			DecimalFormat df = new DecimalFormat("#,###,###", otherSymbols);
			Double a = Double.parseDouble(data);
			String d = df.format(a);
			return d;
		}
	}

	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);
	}

	public static String formatDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		return formatDate(sdf.parse(date));
	}
	
	
/*	public static void main(String[] args) {
		try {
			System.out.println(formatDate("2017-04-27T07:00:00.000+07:00"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}

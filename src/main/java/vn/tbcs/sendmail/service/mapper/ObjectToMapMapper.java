package vn.tbcs.sendmail.service.mapper;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ObjectToMapMapper {
	public static Map<String, Object> introspect(Object obj) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		BeanInfo info = Introspector.getBeanInfo(obj.getClass());
		for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
			Method reader = pd.getReadMethod();
			if (reader != null) {
				String pName = pd.getName();
				char first = Character.toUpperCase(pName.charAt(0));
				pName = first + pName.substring(1);
				//String value = reader.invoke(obj).toString();
				result.put(pName, reader.invoke(obj));
			}
		}
		return result;
	}

	private String formatValue(String value) {
		String result = "";
		
		return result;
	}
}

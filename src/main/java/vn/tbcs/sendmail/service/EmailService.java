package vn.tbcs.sendmail.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tbcs.sendmail.config.AppConfig;
import vn.tbcs.sendmail.model.Contract;
import vn.tbcs.sendmail.model.EmailInfo;
import vn.tbcs.sendmail.repository.BaseDSSRepository;
import vn.tbcs.sendmail.repository.ContractRepository;
import vn.tbcs.sendmail.repository.MailTemplateRepository;

@Service("emailService")
public class EmailService extends BaseDSSRepository {
	static Logger logger = LogManager.getLogger(EmailService.class.getName());
	@Autowired
	private ContractRepository custService;
	@Autowired
	private NotificationServiceImpl notifyService;
	@Autowired
	private MailTemplateRepository mailTemplateRepository;
	@Autowired
	private AppConfig appConfig;

	public void sendEmail() {
		List<Contract> listContract = custService.getCustomers(appConfig.getContractService());
		try {
			if (listContract != null) {
				for (Contract contract : listContract) {
					// DuocRollTiepKhong = 1 --> email 2
					String custEmail = contract.getCustomerEmail();
					if (custEmail != null && "".equals(custEmail) == false) {
						if ("1.0".equals(contract.getDuocRollTiepKhong())) {
							String emailText = mailTemplateRepository.getMailContract1(contract,
									appConfig.getContractTemplate2());
							EmailInfo emailInfo = new EmailInfo(custEmail, "", appConfig.getBccMail(),
									appConfig.getTitle(), emailText, null);
							notifyService.sendEmailWithBBC(emailInfo);
						} else if ("0.0".equals(contract.getDuocRollTiepKhong())) {
							String emailText = mailTemplateRepository.getMailContract1(contract,
									appConfig.getContractTemplate1());
							EmailInfo emailInfo = new EmailInfo(custEmail, "", appConfig.getBccMail(),
									appConfig.getTitle(), emailText, null);
							notifyService.sendEmailWithBBC(emailInfo);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

}

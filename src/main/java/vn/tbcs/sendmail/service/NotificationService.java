package vn.tbcs.sendmail.service;

import vn.tbcs.sendmail.model.EmailInfo;

/**
 * 
 * @author NguyenNam
 * 
 */

public interface NotificationService {

	public int sendEmail(String template, String receiver, String cc, String subject, String content, String fileAttach)
			throws Exception;
	public int sendEmailWithBBC(EmailInfo emailInfo);

}

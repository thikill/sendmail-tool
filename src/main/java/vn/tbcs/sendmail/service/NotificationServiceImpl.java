package vn.tbcs.sendmail.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import vn.tbcs.sendmail.config.AppConfig;
import vn.tbcs.sendmail.model.EmailInfo;
import vn.tbcs.sendmail.model.FileAttachInfo;
import vn.tbcs.sendmail.repository.BaseDSSRepository;
import vn.tbcs.sendmail.repository.utils.algorithm.SimpleParser;
import vn.tbcs.sendmail.repository.utils.context.HttpContext;
import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author NguyenNam
 * 
 */

@Service("notificationService")
public class NotificationServiceImpl extends BaseDSSRepository implements NotificationService, InitializingBean {
	private static final Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);
	private static final String TCBS_PATH = ":TCBS:";
	private static final String TCBS_PATH_FILE = ":TCBS2016:";
	@Autowired
	private AppConfig appConfig;

	private String buildEmailRequest(String template, String receiver, String cc, String subject, String content,
			String file, String uri) throws Exception {
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put("template", StringEscapeUtils.escapeXml(template));
		valueMap.put("receiver", StringEscapeUtils.escapeXml(receiver));
		valueMap.put("cc", StringEscapeUtils.escapeXml(cc));
		valueMap.put("subject", StringEscapeUtils.escapeXml(subject));
		valueMap.put("content", StringEscapeUtils.escapeXml(content));
		valueMap.put("fileAttach", StringEscapeUtils.escapeXml(file));
		// SimpleParser sp = new SimpleParser(getTemplate(uri));
		SimpleParser sp = new SimpleParser(getTemplateInJar(uri));// ThiMB 26/04
																	// change
																	// for
																	// package
																	// all in a
																	// jar file
		String xmlToSend = sp.parse(valueMap);
		return xmlToSend;
	}

	public String getTemplateInJar(String wsUri) {
		String template = "";
		try {
			String templateFilename = SimpleParser.getTemplateName(wsUri);
			logger.info("Template file {}", templateFilename);
			InputStream inputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(templateFilename);
			StringWriter writer = new StringWriter();
			IOUtils.copy(inputStream, writer, "UTF-8");
			template = writer.toString();

		} catch (IOException e) {
			logger.error("Can not load template for message. Exception: " + e.getMessage());
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return template;
	}

	public String getTemplate(String wsUri) {
		String template = "";
		try {
			String templateFilename = SimpleParser.getTemplateName(wsUri);
			logger.info("Template file {}", templateFilename);
			URL fileURL = Thread.currentThread().getContextClassLoader().getResource(templateFilename);

			if (fileURL != null) {
				String filePath = URLDecoder.decode(fileURL.getFile(), "UTF-8");
				logger.debug("Reading soap template from file: " + filePath);
				template = SimpleParser.getTemplate(filePath);
			} else {
				logger.error("Soap template file: " + templateFilename + " could not be found in classpath");
			}
		} catch (IOException e) {
			logger.error("Can not load template for message. Exception: " + e.getMessage());
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return template;
	}

	private String parseSMSResult(String xmlResponse) {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("ns1:sendSMSResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error in while parsing response SMS: " + ex.getMessage());
		}
		return result;
	}

	private String getSMSResult(String xmlResponse) throws Exception {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("ns1:sendSMSResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			logger.error("Error in while parsing response SMS: " + ex.getMessage());
			throw new Exception("Send SMS error");
		}
		return result;
	}

	private String parseEmailResult(String xmlResponse) {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("ns1:sendEmailResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error in while parsing response SMS: " + ex.getMessage());
		}
		return result;
	}

	private static String getCharacterDataFromElement(Element e) throws UnsupportedEncodingException, DOMException {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return new String(cd.getData().getBytes(), "UTF-8");
		}
		return "";
	}

	private String buildEmailRequestWithBbc(EmailInfo emailInfo, String uri) throws IOException {
		Map<String, String> valueMap = new HashMap<String, String>();
		if (!StringUtils.isEmpty(emailInfo.getTemplate())) {
			valueMap.put("template", StringEscapeUtils.escapeXml(emailInfo.getTemplate()));
		} else {
			valueMap.put("template", "");
		}
		valueMap.put("receiver", StringEscapeUtils.escapeXml(emailInfo.getReceiver()));
		if (!StringUtils.isEmpty(emailInfo.getCc())) {
			valueMap.put("cc", StringEscapeUtils.escapeXml(emailInfo.getCc()));
		} else {
			valueMap.put("cc", "");
		}
		valueMap.put("subject", StringEscapeUtils.escapeXml(emailInfo.getSubject()));

		if (!StringUtils.isEmpty(emailInfo.getBcc())) {
			valueMap.put("bcc", StringEscapeUtils.escapeXml(emailInfo.getBcc()));
		}
		valueMap.put("content", StringEscapeUtils.escapeXml(emailInfo.getContent()));

		valueMap.put("fileAttach", "");
		valueMap.put("is64", "no");
		SimpleParser sp = new SimpleParser(getTemplateInJar(uri));
		String xmlToSend = sp.parse(valueMap);
		return xmlToSend;
	}
	private String parseEmailBbcResult(String xmlResponse) {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("ns1:sendEmailWithBccResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error in while parsing response email: " + ex.getMessage());
		}
		return result;
	}
	@Override
	public int sendEmail(String template, String receiver, String cc, String subject, String content, String fileAttach)
			throws Exception {
		String response = "";
		try {
			String xmlToSend = buildEmailRequest(template, receiver, cc, subject, content, fileAttach,
					appConfig.getEmailServiceURL());
			String xmlResponse = httpContext.httpPost(appConfig.getEmailServiceURL(), xmlToSend);
			response = parseEmailResult(xmlResponse);
			logger.info("Email:" + receiver + "@Response:" + response);
		} catch (Exception e) {
			logger.error("Error while send Email:" + receiver + "@Message:" + e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return 0;
	}

	@Override
	public int sendEmailWithBBC(EmailInfo emailInfo) {
		String response = "";
		try {
			String xmlToSend = buildEmailRequestWithBbc(emailInfo, appConfig.getEmailServiceURL());
			//logger.info("Email:" + emailInfo.getReceiver() + "@Request:" + emailInfo.getContent());
			String xmlResponse = httpContext.httpPost(appConfig.getEmailServiceURL(), xmlToSend);
			response = parseEmailBbcResult(xmlResponse);
			logger.info("Email:" + emailInfo.getReceiver() + "@Response:" + response);

		} catch (Exception e) {
			logger.error("Error while send Email:" + emailInfo.getReceiver() + "@Message:" + e.getMessage());
		}
		return 0;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		httpContext.init();
	}
}

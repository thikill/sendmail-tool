package vn.tbcs.sendmail.model;

import java.util.List;

public class EmailInfo {

	private String template;
	private String receiver;
	private String cc;
	private String bcc;
	private String subject;
	private String content;
	private String is64;
	private List<FileAttachInfo> fileAttachs;
	
	
	public EmailInfo(String template, String receiver, String cc, String bcc, String subject, String content,
			String is64, List<FileAttachInfo> fileAttachs) {
		super();
		this.template = template;
		this.receiver = receiver;
		this.cc = cc;
		this.bcc = bcc;
		this.subject = subject;
		this.content = content;
		this.is64 = is64;
		this.fileAttachs = fileAttachs;
	}

	public EmailInfo(String receiver, String cc, String bcc, String subject, String content,
			List<FileAttachInfo> fileAttachs) {
		super();
		this.receiver = receiver;
		this.cc = cc;
		this.bcc = bcc;
		this.subject = subject;
		this.content = content;
		this.fileAttachs = fileAttachs;
	}
	
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getBcc() {
		return bcc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getIs64() {
		return is64;
	}
	public void setIs64(String is64) {
		this.is64 = is64;
	}
	public List<FileAttachInfo> getFileAttachs() {
		return fileAttachs;
	}
	public void setFileAttachs(List<FileAttachInfo> fileAttachs) {
		this.fileAttachs = fileAttachs;
	}

	
	
	
}

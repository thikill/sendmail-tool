package vn.tbcs.sendmail.model;

public class FileAttachInfo {
	
	private String name;
	private String mimeType;
	private String path;
	
	
	public FileAttachInfo(String name, String mimeType, String path) {
		super();
		this.name = name;
		this.mimeType = mimeType;
		this.path = path;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

}

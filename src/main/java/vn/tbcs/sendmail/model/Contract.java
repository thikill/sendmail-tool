package vn.tbcs.sendmail.model;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;


public class Contract {
	private String TradingCode;
	private String BondCode;
	private String ProductCode;
	private String Category;
	private String NgayMua;
	private String SoLuongTP;
	private String MenhGia;
	private String GocDauTuBanDau;

	private String SoLanDuocRoll;
	private String SoLanDaRoll;
	private String DuocRollTiepKhong;

	private String GocDauTuNeuTatToan;
	private String NgayDenHanTatToan;
	private String TienTatToanSeNhan;
	private String CouponDaNhan;
	private String LoiNhuanNeuTatToan;
	private String LoiTucNeuTatToan;
	private String GocDauTuNeuRollTiep;
	private String NgayDenHanRollTiep;
	private String TienRollTiepSeNhan;
	private String CouponSeNhanRollTiep;
	private String LoiNhuanNeuRollTiep;
	private String TaiDauTuRollTiep;
	private String LoiTucNeuRollTiep;
	private String GocDauTuNeuGiuDaoHan;
	private String NgayDaoHan;
	private String TienDaoHanSeNhan;
	private String CouponSeNhanDaoHan;
	private String LoiNhuanNeuGiuDaoHan;
	private String TaiDauTuGiuDaoHan;
	private String LoiTucNeuGiuDaoHan;
	private String CustomerID;
	private String CustomerName;
	private String CustomerEmail;
	private String Title;
	private String RM_Name;
	private String AgencyCode;
	private String RMEmail;
	public String getTradingCode() {
		return TradingCode;
	}
	public void setTradingCode(String tradingCode) {
		TradingCode = tradingCode;
	}
	public String getBondCode() {
		return BondCode;
	}
	public void setBondCode(String bondCode) {
		BondCode = bondCode;
	}
	public String getProductCode() {
		return ProductCode;
	}
	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getNgayMua() {
		return NgayMua;
	}
	public void setNgayMua(String ngayMua) {
		NgayMua = ngayMua;
	}
	public String getSoLuongTP() {
		return SoLuongTP;
	}
	public void setSoLuongTP(String soLuongTP) {
		SoLuongTP = soLuongTP;
	}
	public String getMenhGia() {
		return MenhGia;
	}
	public void setMenhGia(String menhGia) {
		MenhGia = menhGia;
	}
	public String getGocDauTuBanDau() {
		return GocDauTuBanDau;
	}
	public void setGocDauTuBanDau(String gocDauTuBanDau) {
		GocDauTuBanDau = gocDauTuBanDau;
	}
	public String getSoLanDuocRoll() {
		return SoLanDuocRoll;
	}
	public void setSoLanDuocRoll(String soLanDuocRoll) {
		SoLanDuocRoll = soLanDuocRoll;
	}
	public String getSoLanDaRoll() {
		return SoLanDaRoll;
	}
	public void setSoLanDaRoll(String soLanDaRoll) {
		SoLanDaRoll = soLanDaRoll;
	}
	public String getDuocRollTiepKhong() {
		return DuocRollTiepKhong;
	}
	public void setDuocRollTiepKhong(String duocRollTiepKhong) {
		DuocRollTiepKhong = duocRollTiepKhong;
	}
	public String getGocDauTuNeuTatToan() {
		return GocDauTuNeuTatToan;
	}
	public void setGocDauTuNeuTatToan(String gocDauTuNeuTatToan) {
		GocDauTuNeuTatToan = gocDauTuNeuTatToan;
	}
	public String getNgayDenHanTatToan() {
		return NgayDenHanTatToan;
	}
	public void setNgayDenHanTatToan(String ngayDenHanTatToan) {
		NgayDenHanTatToan = ngayDenHanTatToan;
	}
	public String getTienTatToanSeNhan() {
		return TienTatToanSeNhan;
	}
	public void setTienTatToanSeNhan(String tienTatToanSeNhan) {
		TienTatToanSeNhan = tienTatToanSeNhan;
	}
	public String getCouponDaNhan() {
		return CouponDaNhan;
	}
	public void setCouponDaNhan(String couponDaNhan) {
		CouponDaNhan = couponDaNhan;
	}
	public String getLoiNhuanNeuTatToan() {
		return LoiNhuanNeuTatToan;
	}
	public void setLoiNhuanNeuTatToan(String loiNhuanNeuTatToan) {
		LoiNhuanNeuTatToan = loiNhuanNeuTatToan;
	}
	public String getLoiTucNeuTatToan() {
		return LoiTucNeuTatToan;
	}
	public void setLoiTucNeuTatToan(String loiTucNeuTatToan) {
		LoiTucNeuTatToan = loiTucNeuTatToan;
	}
	public String getGocDauTuNeuRollTiep() {
		return GocDauTuNeuRollTiep;
	}
	public void setGocDauTuNeuRollTiep(String gocDauTuNeuRollTiep) {
		GocDauTuNeuRollTiep = gocDauTuNeuRollTiep;
	}
	public String getNgayDenHanRollTiep() {
		return NgayDenHanRollTiep;
	}
	public void setNgayDenHanRollTiep(String ngayDenHanRollTiep) {
		NgayDenHanRollTiep = ngayDenHanRollTiep;
	}
	public String getTienRollTiepSeNhan() {
		return TienRollTiepSeNhan;
	}
	public void setTienRollTiepSeNhan(String tienRollTiepSeNhan) {
		TienRollTiepSeNhan = tienRollTiepSeNhan;
	}
	public String getCouponSeNhanRollTiep() {
		return CouponSeNhanRollTiep;
	}
	public void setCouponSeNhanRollTiep(String couponSeNhanRollTiep) {
		CouponSeNhanRollTiep = couponSeNhanRollTiep;
	}
	public String getLoiNhuanNeuRollTiep() {
		return LoiNhuanNeuRollTiep;
	}
	public void setLoiNhuanNeuRollTiep(String loiNhuanNeuRollTiep) {
		LoiNhuanNeuRollTiep = loiNhuanNeuRollTiep;
	}
	public String getTaiDauTuRollTiep() {
		return TaiDauTuRollTiep;
	}
	public void setTaiDauTuRollTiep(String taiDauTuRollTiep) {
		TaiDauTuRollTiep = taiDauTuRollTiep;
	}
	public String getLoiTucNeuRollTiep() {
		return LoiTucNeuRollTiep;
	}
	public void setLoiTucNeuRollTiep(String loiTucNeuRollTiep) {
		LoiTucNeuRollTiep = loiTucNeuRollTiep;
	}
	public String getGocDauTuNeuGiuDaoHan() {
		return GocDauTuNeuGiuDaoHan;
	}
	public void setGocDauTuNeuGiuDaoHan(String gocDauTuNeuGiuDaoHan) {
		GocDauTuNeuGiuDaoHan = gocDauTuNeuGiuDaoHan;
	}
	public String getNgayDaoHan() {
		return NgayDaoHan;
	}
	public void setNgayDaoHan(String ngayDaoHan) {
		NgayDaoHan = ngayDaoHan;
	}
	public String getTienDaoHanSeNhan() {
		return TienDaoHanSeNhan;
	}
	public void setTienDaoHanSeNhan(String tienDaoHanSeNhan) {
		TienDaoHanSeNhan = tienDaoHanSeNhan;
	}
	public String getCouponSeNhanDaoHan() {
		return CouponSeNhanDaoHan;
	}
	public void setCouponSeNhanDaoHan(String couponSeNhanDaoHan) {
		CouponSeNhanDaoHan = couponSeNhanDaoHan;
	}
	public String getLoiNhuanNeuGiuDaoHan() {
		return LoiNhuanNeuGiuDaoHan;
	}
	public void setLoiNhuanNeuGiuDaoHan(String loiNhuanNeuGiuDaoHan) {
		LoiNhuanNeuGiuDaoHan = loiNhuanNeuGiuDaoHan;
	}
	public String getTaiDauTuGiuDaoHan() {
		return TaiDauTuGiuDaoHan;
	}
	public void setTaiDauTuGiuDaoHan(String taiDauTuGiuDaoHan) {
		TaiDauTuGiuDaoHan = taiDauTuGiuDaoHan;
	}
	public String getLoiTucNeuGiuDaoHan() {
		return LoiTucNeuGiuDaoHan;
	}
	public void setLoiTucNeuGiuDaoHan(String loiTucNeuGiuDaoHan) {
		LoiTucNeuGiuDaoHan = loiTucNeuGiuDaoHan;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	
	public String getCustomerName() {
		if (StringUtils.isEmpty(CustomerName) == false) {
			CustomerName = WordUtils.capitalize(CustomerName.toLowerCase());
		}
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustomerEmail() {
		return CustomerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		CustomerEmail = customerEmail;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getRM_Name() {
		return RM_Name;
	}
	public void setRM_Name(String rM_Name) {
		RM_Name = rM_Name;
	}
	public String getAgencyCode() {
		return AgencyCode;
	}
	public void setAgencyCode(String agencyCode) {
		AgencyCode = agencyCode;
	}
	public String getRMEmail() {
		return RMEmail;
	}
	public void setRMEmail(String rMEmail) {
		RMEmail = rMEmail;
	}

}

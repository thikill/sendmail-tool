package vn.tbcs.sendmail.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${mail.contract.title}")
    private String title;
    @Value("${url.notification.email}")
    private String emailServiceURL;
    
    @Value("${contract.service}")
    private String contractService;
    
    @Value("${contract.template1}")
    private String contractTemplate1;
    
    
    @Value("${contract.template2}")
    private String contractTemplate2;    
    
    @Value("${contract.mail.bcc}")
    private String bccMail;
    

	public String getBccMail() {
		return bccMail;
	}

	public void setBccMail(String bccMail) {
		this.bccMail = bccMail;
	}

	public String getContractService() {
		return contractService;
	}

	public void setContractService(String contractService) {
		this.contractService = contractService;
	}

	public String getContractTemplate1() {
		return contractTemplate1;
	}

	public void setContractTemplate1(String contractTemplate1) {
		this.contractTemplate1 = contractTemplate1;
	}

	public String getContractTemplate2() {
		return contractTemplate2;
	}

	public void setContractTemplate2(String contractTemplate2) {
		this.contractTemplate2 = contractTemplate2;
	}

	public String getEmailServiceURL() {
		return emailServiceURL;
	}

	public void setEmailServiceURL(String emailServiceURL) {
		this.emailServiceURL = emailServiceURL;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

    // ...

}
package vn.tbcs.sendmail;

import java.lang.reflect.Field;
import java.nio.charset.Charset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * Author: ThiMB
 * Send mail tool
 *
 */
@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = { "vn.tbcs.sendmail" })
@PropertySource(value = { "classpath:configs.properties" })
public class MainApp {

	static Logger logger = LogManager.getLogger(MainApp.class.getName());

	public static void main(String[] args) {
		System.setProperty("file.encoding", "UTF-8");
		Field charset;
		try {
			charset = Charset.class.getDeclaredField("defaultCharset");
			charset.setAccessible(true);
			charset.set(null, null);
			SpringApplication.run(MainApp.class, args);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}

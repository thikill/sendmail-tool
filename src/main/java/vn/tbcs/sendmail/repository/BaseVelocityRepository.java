package vn.tbcs.sendmail.repository;

import java.io.StringWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

public class BaseVelocityRepository {
	static Logger logger = LogManager.getLogger(BaseVelocityRepository.class.getName());

	public VelocityEngine getVelocityEngine() {
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		ve.init();
		return ve;
	}

	public String getVelocityContextData(Template template, VelocityContext velocityContext) {
		try {
			StringWriter stringWirter = new StringWriter();
			template.merge(velocityContext, stringWirter);
			return stringWirter.toString();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	public String getContent(String templatePath, Object objInfo) {
		String content = "";
		try {
			Template template = getVelocityEngine().getTemplate(templatePath, "UTF-8");// "template\\email-2.vm"
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("numberTool", new NumberTool());
			velocityContext.put("date", new DateTool());
			velocityContext.put("objInfo", objInfo);
			content = getVelocityContextData(template, velocityContext);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return content;
	}

}

package vn.tbcs.sendmail.repository.utils.context;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.net.HttpHeaders;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;

public class AsyncHttpClientHttpUtil implements HttpUtil {

	private static final Logger logger = LoggerFactory.getLogger(AsyncHttpClientHttpUtil.class);

	private final AsyncHttpClient asyncHttpClient;

	public AsyncHttpClientHttpUtil(AsyncHttpClient asyncHttpClient) {
		this.asyncHttpClient = asyncHttpClient;
	}

	@Override
	public HttpResponse get(String url, Map<String, String> headers, Map<String, String> queries) throws IOException {
		RequestBuilder requestBuilder = buildGetRequest(url, headers, queries);
		logger.debug("Get: {}", url);
		try {
			Response response = asyncHttpClient.executeRequest(requestBuilder.build()).get(100, TimeUnit.SECONDS);
			if (response.getStatusCode() == 302) {
				response = asyncHttpClient.prepareGet(response.getHeader(HttpHeaders.LOCATION)).execute().get();
			}

			return new HttpResponse(response.getStatusCode(), response.getHeaders(), response.getResponseBodyAsBytes());
		} catch (InterruptedException e) {
			logger.warn("Get ERROR", e);
			Thread.currentThread().interrupt();
			return HttpResponse.NULL;
		} catch (ExecutionException e) {
			logger.warn("Get ERROR", e);
			return HttpResponse.NULL;
		} catch (Exception e) {
			logger.warn("Get ERROR", e);
			return HttpResponse.NULL;
		}
	}

	@Override
	public void get(String url, Map<String, String> headers, Map<String, String> queries,
			final HttpOperationListener httpOperationListener) {
		RequestBuilder requestBuilder = buildGetRequest(url, headers, queries);

		try {
			asyncHttpClient.executeRequest(requestBuilder.build(), new AsyncCompletionHandler<Void>() {
				@Override
				public void onThrowable(Throwable t) {
					httpOperationListener.onThrowable(t);
				}

				@Override
				public Void onCompleted(Response response) throws Exception {
					httpOperationListener.onResponse(new HttpResponse(response.getStatusCode(), response.getHeaders(),
							response.getResponseBodyAsBytes()));
					return null;
				}
			});
		} catch (Exception e) {
			httpOperationListener.onThrowable(e);
		}
	}

	private RequestBuilder buildGetRequest(String url, Map<String, String> headers, Map<String, String> queries) {
		RequestBuilder requestBuilder = new RequestBuilder();
		requestBuilder.setUrl(url);
		for (Entry<String, String> header : headers.entrySet()) {
			requestBuilder.setHeader(header.getKey(), header.getValue());
		}

		for (Entry<String, String> query : queries.entrySet()) {
			requestBuilder.addQueryParam(query.getKey(), query.getValue());
		}
		return requestBuilder;
	}

	@Override
	public HttpResponse post(String url, Map<String, String> headers, Map<String, String> params) throws IOException {
		RequestBuilder requestBuilder = buildPostRequest(url, headers, params);

		try {
			Response response = asyncHttpClient.executeRequest(requestBuilder.build()).get(100, TimeUnit.SECONDS);
			return new HttpResponse(response.getStatusCode(), response.getHeaders(), response.getResponseBodyAsBytes());
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			return null;
		} catch (ExecutionException e) {
			return null;
		} catch (Exception e) {
			logger.warn("Get ERROR", e);
			return HttpResponse.NULL;
		}

	}

	@Override
	public void post(String url, Map<String, String> headers, Map<String, String> params,
			final HttpOperationListener httpOperationListener) {
		RequestBuilder requestBuilder = buildPostRequest(url, headers, params);
		try {
			asyncHttpClient.executeRequest(requestBuilder.build(), new AsyncCompletionHandler<Void>() {
				@Override
				public void onThrowable(Throwable t) {
					httpOperationListener.onThrowable(t);
				}

				@Override
				public Void onCompleted(Response response) throws Exception {
					httpOperationListener.onResponse(new HttpResponse(response.getStatusCode(), response.getHeaders(),
							response.getResponseBodyAsBytes()));
					return null;
				}
			});
		} catch (Exception e) {
			httpOperationListener.onThrowable(e);
		}
	}
	
	@Override
	public HttpResponse put(String url, Map<String, String> headers, Map<String, String> params) throws IOException {
		RequestBuilder requestBuilder = buildPutRequest(url, headers, params);

		try {
			Response response = asyncHttpClient.executeRequest(requestBuilder.build()).get(100, TimeUnit.SECONDS);
			return new HttpResponse(response.getStatusCode(), response.getHeaders(), response.getResponseBodyAsBytes());
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			return null;
		} catch (ExecutionException e) {
			return null;
		} catch (Exception e) {
			logger.warn("Get ERROR", e);
			return HttpResponse.NULL;
		}

	}

	@Override
	public void put(String url, Map<String, String> headers, Map<String, String> params,
			final HttpOperationListener httpOperationListener) {
		RequestBuilder requestBuilder = buildPutRequest(url, headers, params);
		try {
			asyncHttpClient.executeRequest(requestBuilder.build(), new AsyncCompletionHandler<Void>() {
				@Override
				public void onThrowable(Throwable t) {
					httpOperationListener.onThrowable(t);
				}

				@Override
				public Void onCompleted(Response response) throws Exception {
					httpOperationListener.onResponse(new HttpResponse(response.getStatusCode(), response.getHeaders(),
							response.getResponseBodyAsBytes()));
					return null;
				}
			});
		} catch (Exception e) {
			httpOperationListener.onThrowable(e);
		}
	}

	private RequestBuilder buildPostRequest(String url, Map<String, String> headers, Map<String, String> params) {
		RequestBuilder requestBuilder = new RequestBuilder();
		requestBuilder.setUrl(url);
		requestBuilder.setMethod("POST");

		for (Entry<String, String> param : params.entrySet()) {
			requestBuilder.addFormParam(param.getKey(), param.getValue());
		}

		for (Entry<String, String> header : headers.entrySet()) {
			requestBuilder.addHeader(header.getKey(), header.getValue());
		}
		return requestBuilder;
	}
	
	private RequestBuilder buildPutRequest(String url, Map<String, String> headers, Map<String, String> params) {
		RequestBuilder requestBuilder = new RequestBuilder();
		requestBuilder.setUrl(url);
		requestBuilder.setMethod("PUT");

		for (Entry<String, String> param : params.entrySet()) {
			requestBuilder.addFormParam(param.getKey(), param.getValue());
		}

		for (Entry<String, String> header : headers.entrySet()) {
			requestBuilder.addHeader(header.getKey(), header.getValue());
		}
		return requestBuilder;
	}

	@Override
	public UploadHandle upload(String urlStr, File file, Map<String, String> headers,
			final UploadListener uploadListener) {
		Preconditions.checkNotNull(uploadListener, "uploadListener null");

		final AtomicBoolean canceled = new AtomicBoolean();
		UploadHandle uploadHandle = new UploadHandle() {
			@Override
			public void cancel() {
				canceled.set(true);
			}
		};
		final String end = "\r\n";
		final String twoHyphens = "--";
		final String boundary = "*****++++++************++++++++++++";

		try {
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);

			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			for (Entry<String, String> header : headers.entrySet()) {
				conn.addRequestProperty(header.getKey(), header.getValue());
			}

			DataOutputStream ds = new DataOutputStream(conn.getOutputStream());
			ds.writeBytes(twoHyphens + boundary + end);
			ds.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"" + file.getName() + "\"" + end);
			ds.writeBytes(end);

			FileInputStream fStream = new FileInputStream(file);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int length = -1;

			while ((length = fStream.read(buffer)) != -1 && !canceled.get()) {
				ds.write(buffer, 0, length);
			}

			ds.writeBytes(end);
			ds.writeBytes(twoHyphens + boundary + twoHyphens + end);
			/* close streams */
			fStream.close();
			ds.flush();
			ds.close();

			int responseCode = conn.getResponseCode();
			InputStream is = conn.getInputStream();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			DataOutputStream outds = new DataOutputStream(out);
			byte[] data = new byte[bufferSize];
			int leng = -1;
			while ((leng = is.read(data)) != -1) {
				outds.write(data, 0, leng);
			}

			uploadListener.completed(
					new HttpResponse(responseCode, Collections.<String, List<String>> emptyMap(), out.toByteArray()));
		} catch (Exception e) {
			uploadListener.exception(e);
			// e.printStackTrace();
		}

		return uploadHandle;
	}
}

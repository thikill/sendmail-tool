package vn.tbcs.sendmail.repository.utils.algorithm;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SignatureUtils {

	private static Log logger = LogFactory.getLog(SignatureUtils.class);
	KeyStore keyStore;
	FileInputStream fis = null;
	FileInputStream fis1 = null;
	X509Certificate cert = null;
	static PrivateKey privateKey = null;
	String PrivKeyPath = "D:\\workspace\\wsapi-gate-card\\target\\classes\\vmg_server.p12";
	String PwdPrivKey = "123456";
	static PublicKey publicKey = null;
	String PubKeyPath = "D:\\workspace\\wsapi-gate-card\\target\\classes\\vmg_server.crt";
	Signature sig = null;

	public SignatureUtils() {
		try {

			/* BEGIN GEN SIGNATURE */

			keyStore = KeyStore.getInstance("PKCS12");
			/*
			 * URL fileURL = Thread.currentThread().getContextClassLoader()
			 * .getResource(PrivKeyPath); if (fileURL != null) { fis = new
			 * FileInputStream(fileURL.getFile()); }
			 */
			fis = new FileInputStream(PrivKeyPath);
			fis1 = new FileInputStream(PubKeyPath);
			keyStore.load(fis, PwdPrivKey.toCharArray());
			keyStore.load(fis1, PwdPrivKey.toCharArray());

			Enumeration en = keyStore.aliases();
			String alias = "";
			Vector vectaliases = new Vector();

			while (en.hasMoreElements()) {
				vectaliases.add(en.nextElement());
				String[] aliases = (String[]) (vectaliases
						.toArray(new String[0]));
				for (int i = 0; i < aliases.length; i++)
					if (keyStore.isKeyEntry(aliases[i])) {
						alias = aliases[i];
						break;
					}
			}
			cert = (X509Certificate) keyStore.getCertificate(alias);
			privateKey = (PrivateKey) keyStore.getKey(alias, PwdPrivKey
					.toCharArray());

			publicKey = (PublicKey) keyStore.getKey(alias, PwdPrivKey
					.toCharArray());
			Provider p = keyStore.getProvider();
			sig = Signature.getInstance("SHA1withRSA", p);
		} catch (Exception e) {
			logger.error("Error in while signature data " + e.getMessage());
			e.printStackTrace();
		}
	}

	public byte[] signData(byte[] data, PrivateKey key) throws Exception {
		// Signature signer = Signature.getInstance("SHA1withDSA");
		sig.initSign(key);
		sig.update(data);
		return (sig.sign());
	}

	public boolean verifySig(byte[] data, PublicKey key, byte[] sigData)
			throws Exception {
		// Signature signer = Signature.getInstance("SHA1withDSA");
		sig.initVerify(key);
		sig.update(data);
		return (sig.verify(sigData));
	}

/*	public static void main(String[] args) throws UnsupportedEncodingException,
			Exception {
		SignatureUtils abc = new SignatureUtils();
		String UserName = "builacviet";
		String Serial = "FA00000036";
		String Pin = "1234567890";
		String PartnerID = "35";
		String ClientIP = "1.1.1.1";
		String SecretKey = "7w0dg55vym4mebhjoy4hhvb3rvljg4ug";
		String OriginData = UserName + Serial + PartnerID + Pin + SecretKey;
		byte[] sigData = abc.signData(OriginData.getBytes("UTF-8"), privateKey);
		System.out.println(new String(abc.signData(
				OriginData.getBytes("UTF-8"), privateKey)));
		// System.out.println(abc.verifySig(OriginData.getBytes("UTF-8"),
		// publicKey, sigData));
	}*/
}

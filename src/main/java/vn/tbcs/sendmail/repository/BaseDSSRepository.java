package vn.tbcs.sendmail.repository;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import vn.tbcs.sendmail.repository.utils.context.HttpContext;

public class BaseDSSRepository {
	static Logger logger = LogManager.getLogger(BaseDSSRepository.class.getName());
	protected HttpContext httpContext;
	protected String urlService;

	public BaseDSSRepository() {
		httpContext = new HttpContext();
		httpContext.setConnectionTimeout(90);
		httpContext.setSocketTimeout(120);
		httpContext.setDefaultMaxConnectionPerHost(100);
		httpContext.setMaxTotalConnection(10000);
		try {
			httpContext.init();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

}

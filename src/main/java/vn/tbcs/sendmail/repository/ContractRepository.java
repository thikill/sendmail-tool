package vn.tbcs.sendmail.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import vn.tbcs.sendmail.model.*;

@Repository
public class ContractRepository extends BaseDSSRepository {
	static Logger logger = LogManager.getLogger(ContractRepository.class.getName());
	private Gson gson;
	private JsonParser parser;

	public ContractRepository() {
		gson = new Gson();
		parser = new JsonParser();
	}

	public List<Contract> getCustomers(String urlService) {
		List<Contract> listCustomer = new ArrayList<>();
		try {
			String result = httpContext.httpGet(urlService, "");
			JsonObject jsonObject = parser.parse(result).getAsJsonObject();
			JsonArray jsonCustArray = null;
			JsonElement rootE = jsonObject.get("TCBond_ThongTinHDDenHanTatToanCollection");
			if (rootE != null) {
				JsonObject jsonObj = rootE.getAsJsonObject();
				if (jsonObj != null) {
					JsonElement jsonE = jsonObj.get("TCBond_ThongTinHDDenHanTatToan");
					if (jsonE != null) {
						jsonCustArray = jsonE.getAsJsonArray();
					}
				}
			}
			if (jsonCustArray != null) {
				for (JsonElement jsonElement : jsonCustArray) {
					Contract obj = gson.fromJson(jsonElement, Contract.class);
					listCustomer.add(obj);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return listCustomer;
	}
}

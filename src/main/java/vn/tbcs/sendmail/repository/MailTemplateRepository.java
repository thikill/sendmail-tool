package vn.tbcs.sendmail.repository;

import org.springframework.stereotype.Repository;

import vn.tbcs.sendmail.model.Contract;

@Repository
public class MailTemplateRepository extends BaseVelocityRepository {
	public String getMailContract2(Contract contract, String templatePath) {
		return getContent(templatePath, contract);
	}

	public String getMailContract1(Contract contract, String templatePath) {
		return getContent(templatePath, contract);
	}

}

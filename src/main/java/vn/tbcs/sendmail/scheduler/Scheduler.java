package vn.tbcs.sendmail.scheduler;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import vn.tbcs.sendmail.service.EmailService;

@Service("scheduler")
public class Scheduler {
	static Logger logger = LogManager.getLogger(Scheduler.class.getName());
	@Autowired
	private EmailService emailService;

	@Scheduled(cron = "${scheduling.job.cron}")
	public void sendMail() {
		logger.info("Start send mail to customer");
		emailService.sendEmail();
		logger.info("End send mail to customer");
	}
}
